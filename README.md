Sample web application based on Spring Boot and AngujarJS.

Master branch:
[![pipeline status](https://gitlab.com/javier.sedano/gata/badges/master/pipeline.svg)](https://gitlab.com/javier.sedano/gata/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.gata)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.gata&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.gata)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.gata)
</details>


(C) Javier Sedano 2013-2019.

Instructions:

  * Download master or clone repo
  * To quickly run it: `gradlew clean createDbWithSampleData bootRun` and browse [gata](http://localhost:8080/gata).
    * Administrator: `a`, password `a`.
    * Plain user: `b`, password `b`.
  * To genenerate deliverables (remember to create database and edit application.properties):
    * To generate an autorunnable jar: `gradlew bootJar`.
    * To generate a deployable war: `gradlew bootWar`.
    * To create a docker image:
      * `set GATA_PWHASH=<the bcrypt hash of the password>`
      * `gradlew deliverDocker`
      * `cd build`
      * `cd docker`
      * Build using `Dockerfile`: `docker build -t gata .`
      * Export docker image to file: `docker save gata | gzip > ../gata-docker.tgz`
  * To study it: import as gradle project into IntelliJ (enable auto-import) or other IDE. Loombook pluging is strongly suggested (see instructions in build.gradle). Interesting gradle tasks to study: clean, build, test, testJs, createDb, createDbWithSampleData, browseDb, sonarqube
    * Launch in IDE with `-Dspring.config.location=src/test/resources/test.properties`. To emulate the deployed behaviour add `-Xmx70m -XX:MaxMetaspaceSize=70m`

  * ~~Master branch is continuously deployed to [https://jsedano.duckdns.org/gata](https://jsedano.duckdns.org/gata). Notice that different passwords are used there, so you may not be able to login. Continuous deployement uses SSH keys and default passwords that are not shown in source. Notice that it is deployed in a severely limited virtual machine, so performance may not be good or may not even work.~~ This server is being used for another project, so only  branch "cd" is continuously deployed.

Keywords: spring, spring-boot, spring-security, blowfish, spring-data, jpa, buildinfo, gradle, lombok, junit, mockmvc, mockito, git, sonar, sonarcloud, jacoco, flyway, hsqldb, thymeleaf, webjars, angularjs, jasmine, karma, istanbul, docker, continuous integration, coninous quality, continuous deployment, gitlab ci, i18n, themes, w3css, responsive, html5, infinite-scroll

TODO: users tab, cars tab, split java/js projects, instrumentation, docker java:8-alpine, docker mix build/testJs/quality

Functional ideas: feedback press, mark selected in list, complete hover in list, autosave
