describe("fw", function () {
  beforeEach(module('gataApp'));

  var fw;
  var log;

  beforeEach(inject(function($injector) {
    fw = $injector.get('fw');
    log = $injector.get('$log');
    jasmine.clock().install();
  }));

  afterEach(function() {
    jasmine.clock().uninstall();
  });

  it("Asserts dummy YES", function () {
    expect(true).toBe(true);
  });

  it("Dumps variable to alert", function() {
    spyOn(window, 'alert');
    fw.debug.var_dump({a: "aa", b: "bb"});
    fw.debug.var_dump({});
    expect(alert.calls.argsFor(0)[0]).toContain("a: aa");
    expect(alert.calls.argsFor(0)[0]).toContain("b: bb");
    expect(alert.calls.argsFor(1)[0]).toBe("");
  });

  it("Dumps variable to console", function() {
    spyOn(log, 'debug');
    fw.debug.var_console({a: "aa", b: "bb"});
    fw.debug.var_console({});
    expect(log.debug.calls.argsFor(0)[0]).toContain("a: aa");
    expect(log.debug.calls.argsFor(0)[0]).toContain("b: bb");
    expect(log.debug.calls.argsFor(1)[0]).toBe("");
  });

  it("Makes one single call", function() {
    var TAG = "mytag";
    var one = 0;
    function f1() {
      setTimeout(function() {
        one = 1;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    fw.ajax.nonConcurrentExec(TAG, f1);
    jasmine.clock().tick(1001);
    expect(one).toBe(1);
    expect(fw.ajax.nonConcurrentExecTags).not.toContain(TAG);
  });

  it("Makes two concurrent calls, queues second", function() {
    var TAG = "mytag";
    var one = 0;
    var two = 0;
    function f1() {
      setTimeout(function() {
        one = 1;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    function f2() {
      setTimeout(function() {
        two = 2;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    fw.ajax.nonConcurrentExec(TAG, f1);
    jasmine.clock().tick(333);
    fw.ajax.nonConcurrentExec(TAG, f2);
    jasmine.clock().tick(1001);
    expect(one).toBe(1);
    expect(two).toBe(0);
    jasmine.clock().tick(1001);
    expect(two).toBe(2);
    expect(fw.ajax.nonConcurrentExecTags).not.toContain(TAG);
  });

  it("Makes three concurrent calls, queues second, third replaces second", function() {
    var TAG = "mytag";
    var one = 0;
    var two = 0;
    var three = 0;
    function f1() {
      setTimeout(function() {
        one = 1;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    function f2() {
      setTimeout(function() {
        two = 2;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    function f3() {
      setTimeout(function() {
        three = 3;
        fw.ajax.nonConcurrentExecFinish(TAG);
      }, 1000);
    }
    fw.ajax.nonConcurrentExec(TAG, f1);
    jasmine.clock().tick(333);
    fw.ajax.nonConcurrentExec(TAG, f2);
    jasmine.clock().tick(333);
    fw.ajax.nonConcurrentExec(TAG, f3);
    jasmine.clock().tick(1001);
    expect(one).toBe(1);
    expect(two).toBe(0);
    expect(three).toBe(0);
    jasmine.clock().tick(1001);
    expect(two).toBe(0);
    expect(three).toBe(3);
    expect(fw.ajax.nonConcurrentExecTags).not.toContain(TAG);
  });

  it("Makes one single call which fails", function() {
    var TAG = "mytag";
    var one = 0;
    var EXPECTED_ERROR_DATA = {status: "422", message: "horror"};
    var EXPECTED_RESPONSE = {status: 42, data: EXPECTED_ERROR_DATA};
    var received_response = null;
    fw.ajax.onError(function(errorData) {
      received_response = errorData;
    });
    function f1() {
      setTimeout(function() {
        one = 1;
        fw.ajax.nonConcurrentExecFinishError(TAG, EXPECTED_RESPONSE);
      }, 1000);
    }
    fw.ajax.nonConcurrentExec(TAG, f1);
    jasmine.clock().tick(1001);
    expect(one).toBe(1);
    expect(fw.ajax.nonConcurrentExecTags).not.toContain(TAG);
    expect(received_response).toBe(EXPECTED_ERROR_DATA);
  });

});
