describe("notesListController", function() {
  var NOTES_URL = "notes";

  beforeEach(module('gataApp'));

  var $controller, $rootScope, $window, $httpBackend, fw;

  function getControllerScope() {
    var $scope = $rootScope.$new();
    var controller = $controller('notesListController', {
     $scope: $scope
    });
    return $scope;
  }

  beforeEach(inject(function($injector) {
    $controller = $injector.get('$controller');
    $rootScope = $injector.get('$rootScope');
    $window = $injector.get('$window');
    $httpBackend = $injector.get('$httpBackend');
    fw = $injector.get('fw');
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  function expectFullyRefreshedOnXXX(scope, xxx) {
    var NOTE_LIST = [
      {id: 42, title: "Cuarentaidos"},
      {id: 43, title: "Cuarentaitres"},
      {id: 44, title: "Cuarentaicuatro"},
      {id: 45, title: "Cuarentaicinco"}
    ];
    $httpBackend.expectGET(createFilterUrl("", 0)).respond(NOTE_LIST);
    expect(scope.fetching).toEqual(false);
    xxx();
    $httpBackend.flush();
    expect(scope.notes).toEqual(NOTE_LIST);
    expect(scope.fetching).toEqual(false);
  }

  function expectFullyRefreshedAutomatically(scope) {
    expectFullyRefreshedOnXXX(scope, function(){});
  }

  function getRefreshedControllerScope() {
    var scope = getControllerScope();
    expectFullyRefreshedAutomatically(scope);
    return scope;
  }

  function createFilterUrl(filter, from) {
    return NOTES_URL + "?filter=" + filter + "&from=" + from;
  }

  it("Refreshes at start", function() {
    var scope = getRefreshedControllerScope();
  });

  it("Refreshes on refreshClicked", function() {
    var scope = getRefreshedControllerScope();
    expectFullyRefreshedOnXXX(scope, function() {
      scope.refreshClicked();
      expect(scope.fetching).toEqual(true);
    });
  });

  function refreshOnFilterChanged(scope) {
    var FILTERED_NOTE_LIST = [
      {id: 42, title: "Cuarentaidos"}
    ];
    $httpBackend.expectGET(createFilterUrl("dos", 0)).respond(FILTERED_NOTE_LIST);
    scope.filter = "dos";
    $httpBackend.flush();
    expect(scope.notes).toEqual(FILTERED_NOTE_LIST);
  }

  it("Refreshes on filter change", function() {
    var scope = getRefreshedControllerScope();
    refreshOnFilterChanged(scope);
  });

  it("Cleans on cleanClicked", function() {
    var scope = getRefreshedControllerScope();
    refreshOnFilterChanged(scope);
    expectFullyRefreshedOnXXX(scope, function() {
      scope.cleanClicked();
    });
  });

  it("Fetches more pages", function() {
    var scope = getRefreshedControllerScope();
    var FILTERED_NOTE_LIST = [
      {id: 42, title: "Cuarentaidos"},
      {id: 43, title: "Cuarentaitres"},
      {id: 44, title: "Cuarentaicuatro"},
      {id: 45, title: "Cuarentaicinco"},
      {id: 46, title: "Cuarentaiseis"}
    ];
    $httpBackend.expectGET(createFilterUrl("", 0)).respond(FILTERED_NOTE_LIST.slice(0, 2));
    scope.refreshClicked();
    $httpBackend.flush();
    expect(scope.notes).toEqual(FILTERED_NOTE_LIST.slice(0, 2));
    $httpBackend.expectGET(createFilterUrl("", 2)).respond(FILTERED_NOTE_LIST.slice(2, 4));
    scope.loadNextPage();
    $httpBackend.flush();
    expect(scope.notes).toEqual(FILTERED_NOTE_LIST.slice(0, 4));
    $httpBackend.expectGET(createFilterUrl("", 4)).respond(FILTERED_NOTE_LIST.slice(4, 6));
    scope.loadNextPage();
    $httpBackend.flush();
    expect(scope.notes).toEqual(FILTERED_NOTE_LIST);
  });


  it("Detects error in refresh", function() {
    var scope = getRefreshedControllerScope();
    var received_response = null;
    fw.ajax.onError(function(errorData) {
      received_response = errorData;
    });
    var EXPECTED_ERROR_DATA = {status: "500", message: "aggg"};
    $httpBackend.expectGET(createFilterUrl("pleaseFail", 0)).respond(500, EXPECTED_ERROR_DATA);
    scope.filter = "pleaseFail";
    $httpBackend.flush();
    expect(scope.fetching).toBe(false);
    expect(received_response.status).toBe(EXPECTED_ERROR_DATA.status);
    expect(received_response.message).toBe(EXPECTED_ERROR_DATA.message);
  });

  it("Adds note on addClicked", function() {
    var scope = getRefreshedControllerScope();
    $httpBackend.expectPOST(NOTES_URL)
      .respond("51");
    var NOTE_LIST = [
      {id: 42, title: "Cuarentaidos"},
      {id: 43, title: "Cuarentaitres"},
      {id: 44, title: "Cuarentaicuatro"},
      {id: 45, title: "Cuarentaicinco"},
      {id: 51, title: "Cincuentaiuno"}
    ];
    $httpBackend.expectGET(createFilterUrl("", 0)).respond(NOTE_LIST);
    var NOTE51 = {id: 51, title: "Cincuentaiuno", content: "What's the question"};
    $httpBackend.expectGET(NOTES_URL + "/51").respond(NOTE51);
    scope.addClicked();
    $httpBackend.flush();
    expect(scope.selectedNote).toEqual(NOTE51);
  });

  it("Shows note on clicked", function() {
    var scope = getRefreshedControllerScope();
    var NOTE42 = {id: 42, title: "Cuarentaidos", content: "The ultimate answer"};
    $httpBackend.expectGET(NOTES_URL + "/42").respond(NOTE42);
    scope.noteClicked(42);
    $httpBackend.flush();
    expect(scope.selectedNote).toEqual(NOTE42);
  });

  it("Fails on clicking non existing note", function() {
    var EXPECTED_ERROR_DATA = {status: "404", message: "nopes"};
    var received_response = null;
    fw.ajax.onError(function(errorData) {
      received_response = errorData;
    });
    var scope = getRefreshedControllerScope();
    $httpBackend.expectGET(NOTES_URL + "/41").respond(404, EXPECTED_ERROR_DATA);
    scope.noteClicked(41);
    $httpBackend.flush();
    expect(scope.selectedNote).toBe(null);
    expect(received_response.status).toBe(EXPECTED_ERROR_DATA.status);
    expect(received_response.message).toBe(EXPECTED_ERROR_DATA.message);
  });

  it("Updates note on saveClicked", function() {
    var scope = getRefreshedControllerScope();
    var NOTE42 = {id: 42, title: "XLII", content: "Lorem ipsum dolor sit amet"};
    scope.selectedNote = NOTE42;
    $httpBackend.expectPUT(NOTES_URL).respond(200);
    var NOTE_LIST = [
      {id: 42, title: "XLII"},
      {id: 43, title: "Cuarentaitres"},
      {id: 44, title: "Cuarentaicuatro"},
      {id: 45, title: "Cuarentaicinco"}
    ];
    $httpBackend.expectGET(createFilterUrl("", 0)).respond(NOTE_LIST);
    scope.saveClicked();
    $httpBackend.flush();
    expect(scope.notes).toEqual(NOTE_LIST);
  });

  it("Deletes note on deleteClicked", function() {
    var scope = getRefreshedControllerScope();
    var NOTE42 = {id: 42, title: "Cuarentaidos", content: "The ultimate answer"};
    scope.selectedNote = NOTE42;
    $httpBackend.expectDELETE(NOTES_URL + "/42").respond(200);
    var NOTE_LIST = [
      {id: 43, title: "Cuarentaitres"},
      {id: 44, title: "Cuarentaicuatro"},
      {id: 45, title: "Cuarentaicinco"}
    ];
    $httpBackend.expectGET(createFilterUrl("", 0)).respond(NOTE_LIST);
    scope.deleteClicked();
    $httpBackend.flush();
    expect(scope.selectedNote).toBe(null);
    expect(scope.notes).toEqual(NOTE_LIST);
  });


});
