describe("ajaxErrorController", function() {

  beforeEach(module('gataApp'));

  var $controller, $rootScope, fw;

  function getControllerScope() {
    var $scope = $rootScope.$new();
    var controller = $controller('ajaxErrorController', {
     $scope: $scope
    });
    return $scope;
  }

  beforeEach(inject(function($injector) {
    $controller = $injector.get('$controller');
    $rootScope = $injector.get('$rootScope');
    fw = $injector.get('fw');
  }));

  it("Shows and hides content on error", function() {
    var scope = getControllerScope();
    var EXPECTED_MESSAGE = "e" + Math.random();
    var EXPECTED_ERROR_DATA = {
        message: EXPECTED_MESSAGE
    };
    var response = {
      data: EXPECTED_ERROR_DATA
    };
    fw.ajax.httpError(response);
    expect(scope.errorData).toEqual(EXPECTED_ERROR_DATA);
    scope.hideError();
    expect(scope.errorData).toEqual(null);
  });

});
