package com.odroid.gata.unit.common;

import com.odroid.gata.common.GataBuildProperties;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.info.BuildProperties;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class GataBuildPropertiesUnitTest {

    @Mock
    private BuildProperties buildProperties;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testEmpty() {
        GataBuildProperties gataBuildProperties = new GataBuildProperties(null);
        String info = gataBuildProperties.toString();
        assertEquals("Gata development version", info);
    }

    @Test
    public void testFilled() {
        Instant now = Instant.now();
        when(buildProperties.getName()).thenReturn("Kitty");
        when(buildProperties.getVersion()).thenReturn("666");
        when(buildProperties.getTime()).thenReturn(now);
        GataBuildProperties gataBuildProperties = new GataBuildProperties(buildProperties);
        String info = gataBuildProperties.toString();
        assertEquals("Kitty 666 @ " + now, info);
    }
}
