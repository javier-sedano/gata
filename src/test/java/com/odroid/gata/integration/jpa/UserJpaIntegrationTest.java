package com.odroid.gata.integration.jpa;

import com.odroid.gata.SpringBootApp;
import com.odroid.gata.domain.Authority;
import com.odroid.gata.domain.User;
import com.odroid.gata.domain.UserAuthority;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class UserJpaIntegrationTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserJpaIntegrationTest.class);

    private static final Long AUSER_ID = 1L;
    private static final String AUSER_USERNAME = "a";

    @PersistenceContext
    private EntityManager em;

    @Test
    @Transactional
    public void testMinimalUser() {
        User aUser = em.find(User.class, AUSER_ID);
        assertEquals(AUSER_USERNAME, aUser.getUsername());
        List<UserAuthority> authorities = aUser.getUserAuthorities();
        LOGGER.debug("A user: {}", aUser);
        assertTrue(
                authorities.stream().map(UserAuthority::getAuthority)
                        .anyMatch(Authority.ROLE_USER::equals));
        assertTrue(
                authorities.stream().map(UserAuthority::getAuthority)
                        .anyMatch(Authority.ROLE_ADMIN::equals));
    }

    @Test
    @Transactional
    public void testSimpleJpaOperations() {
        User zUser = new User("z", "qwerty", true, "Zacarias");
        em.persist(zUser);
        User gotZUser = em.find(User.class, zUser.getId());
        assertEquals(zUser.getUsername(), gotZUser.getUsername());
        assertEquals(zUser.getHash(), gotZUser.getHash());
        assertEquals(zUser.getEnabled(), gotZUser.getEnabled());
        assertEquals(zUser.getName(), gotZUser.getName());
        assertEquals(zUser, gotZUser);
    }

}
