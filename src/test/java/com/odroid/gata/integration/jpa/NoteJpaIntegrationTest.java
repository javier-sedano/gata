package com.odroid.gata.integration.jpa;

import com.odroid.gata.SpringBootApp;
import com.odroid.gata.domain.Note;
import com.odroid.gata.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class NoteJpaIntegrationTest {

    private static final String NOTE_TITLE = "T1tl3";
    private static final String NOTE_CONTENT = "C0nt3nt";
    private static final Long AUSER_ID = 1L;

    @PersistenceContext
    private EntityManager em;

    @Test
    @Transactional
    public void testSimpleNoteNavigation() {
        User aUser = em.find(User.class, AUSER_ID);
        Note note1 = new Note(NOTE_TITLE, NOTE_CONTENT, aUser);
        em.persist(note1);
        Note gotNote1 = aUser.getNotes().stream().filter(note -> note.getId().equals(note1.getId()))
                .findFirst().orElseThrow(IllegalStateException::new);
        assertEquals(NOTE_TITLE, gotNote1.getTitle());
        assertEquals(NOTE_CONTENT, gotNote1.getContent());
        assertEquals(note1.getId(), gotNote1.getId());
        assertEquals(AUSER_ID, gotNote1.getUser().getId());
    }
}
