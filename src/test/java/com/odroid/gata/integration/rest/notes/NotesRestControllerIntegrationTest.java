package com.odroid.gata.integration.rest.notes;

import com.odroid.gata.SpringBootApp;
import com.odroid.gata.common.GataConfiguration;
import com.odroid.gata.domain.Note;
import com.odroid.gata.domain.User;
import com.odroid.gata.rest.notes.NotesRestController;
import com.odroid.gata.service.iface.notes.DetailedNote;
import com.odroid.gata.service.iface.notes.MinimalNote;
import com.odroid.gata.service.impl.ForbiddenException;
import com.odroid.gata.service.impl.NotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.odroid.gata.integration.IntegrationTestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class NotesRestControllerIntegrationTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private NotesRestController notesController;

    @Autowired
    private GataConfiguration gataConfiguration;

    @Test
    @Transactional
    public void testFindMyNotes() {
        assertEquals(
                new ArrayList<>(),
                notesController.find("", 0, adminPrincipal()));
        User admin = em.find(User.class, ID_ADMIN);
        User user = em.find(User.class, ID_USER);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        Note note2 = new Note("asdf", "ghjkl", admin);
        em.persist(note2);
        Note note3 = new Note("ASDF", "GHJKL", admin);
        em.persist(note3);
        Note note4 = new Note("asdf", "ghjkl", user);
        em.persist(note4);
        assertEquals(
                new ArrayList<>(),
                notesController.find("zxcvbnm", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("q", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("r", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("we", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("t", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("p", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note1)),
                notesController.find("ui", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note3), MinimalNote.fromNote(note2)),
                notesController.find("asdf", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note3),
                        MinimalNote.fromNote(note2), MinimalNote.fromNote(note1)),
                notesController.find("", 0, adminPrincipal())
        );
        assertEquals(
                Arrays.asList(MinimalNote.fromNote(note4)),
                notesController.find("asdf", 0, userPrincipal())
        );
    }

    @Test
    @Transactional
    public void testFindMyNotesWithPagination() {
        assertEquals(
                new ArrayList<>(),
                notesController.find("", 0, adminPrincipal()));
        User admin = em.find(User.class, ID_ADMIN);
        List<MinimalNote> insertedMinimalNotes = new ArrayList<>();
        for (int i = 0; i < 10 * gataConfiguration.getPageSize(); i++) {
            Note note = new Note(String.format("title%03d", i), String.format("content%03d", i), admin);
            em.persist(note);
            insertedMinimalNotes.add(MinimalNote.fromNote(note));
        }
        List<MinimalNote> gotMinimalNotes = notesController.find("title", 0, adminPrincipal());
        assertEquals(
                insertedMinimalNotes.subList(0, (int) gataConfiguration.getPageSize()),
                notesController.find("title", 0, adminPrincipal())
        );
        assertEquals(
                insertedMinimalNotes.subList((int) gataConfiguration.getPageSize(),
                        (int) gataConfiguration.getPageSize() + (int) gataConfiguration.getPageSize()),
                notesController.find("title", (int) gataConfiguration.getPageSize(), adminPrincipal())
        );
        assertEquals(
                insertedMinimalNotes.subList(insertedMinimalNotes.size() - 3, insertedMinimalNotes.size()),
                notesController.find("title", insertedMinimalNotes.size() - 3, adminPrincipal())
        );
    }

    @Test
    @Transactional
    public void testGetNote() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        DetailedNote gotDetailedNote1 = notesController.retrieve(note1.getId(), adminPrincipal());
        assertEquals(DetailedNote.fromNote(note1), gotDetailedNote1);
    }

    @Test(expected = NotFoundException.class)
    @Transactional
    public void testGetNoteNotExisting() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        notesController.retrieve(note1.getId() + 1, adminPrincipal());
    }

    @Test(expected = ForbiddenException.class)
    @Transactional
    public void testGetNoteHacker() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        notesController.retrieve(note1.getId(), userPrincipal());
    }

    @Test
    @Transactional
    public void testCreate() {
        Long noteId = notesController.create(adminPrincipal());
        Note gotNote = em.find(Note.class, noteId);
        assertEquals("Lorem ipsum", gotNote.getTitle());
        assertEquals("dolor sit amet", gotNote.getContent());
    }

    @Test
    @Transactional
    public void testUpdate() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        DetailedNote detailedNote =
                new DetailedNote(note1.getId(), "Lorem ipsum", "dolor sit amet");
        notesController.update(detailedNote, adminPrincipal());
        Note gotNote = em.find(Note.class, note1.getId());
        assertEquals("Lorem ipsum", gotNote.getTitle());
        assertEquals("dolor sit amet", gotNote.getContent());
    }

    @Test(expected = ForbiddenException.class)
    @Transactional
    public void testUpdateHacker() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        DetailedNote detailedNote =
                new DetailedNote(note1.getId(), "Lorem ipsum", "dolor sit amet");
        notesController.update(detailedNote, userPrincipal());
    }

    @Test
    @Transactional
    public void testDelete() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        notesController.delete(note1.getId(), adminPrincipal());
        assertNull(em.find(Note.class, note1.getId()));
    }

    @Test(expected = ForbiddenException.class)
    @Transactional
    public void testDeleteHacker() {
        User admin = em.find(User.class, ID_ADMIN);
        Note note1 = new Note("qwer", "tyuiop", admin);
        em.persist(note1);
        notesController.delete(note1.getId(), userPrincipal());
    }

}