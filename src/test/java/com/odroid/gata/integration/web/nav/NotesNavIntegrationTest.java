package com.odroid.gata.integration.web.nav;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.odroid.gata.SpringBootApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.odroid.gata.integration.web.WebIntegrationTestUtils.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG"
        })
@TestPropertySource("classpath:test.properties")
public class NotesNavIntegrationTest {

    private static final String NOTES_URL = "/notes";
    private static final String NOTES_FILTER_PARAM = "filter";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private MockMvc mockMvc;

    private ObjectMapper jackson = new ObjectMapper();

    @Before
    public void configureMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void testNotesForAll() throws Exception {
        mockMvc.perform(
                get(NOTESTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(status().isOk());
        mockMvc.perform(
                get(NOTESTAB_URL).with(testUser()))
                .andDo(log())
                .andExpect(status().isOk());
    }

}
