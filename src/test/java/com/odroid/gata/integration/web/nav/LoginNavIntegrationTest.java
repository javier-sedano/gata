package com.odroid.gata.integration.web.nav;

import static com.odroid.gata.integration.web.WebIntegrationTestUtils.ANGULARJS_URL;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.FAVICON_URL;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.LOGIN_URL;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.LOGOUT_URL;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.NOTESTAB_URL;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.PASSWORD_PARAM;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.PASSWRD_ADMIN_KO;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.PASSWRD_ADMIN_OK;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.PASSWRD_USER_KO;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.PASSWRD_USER_OK;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.USERNAME_ADMIN_OK;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.USERNAME_PARAM;
import static com.odroid.gata.integration.web.WebIntegrationTestUtils.USERNAME_USER_OK;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.odroid.gata.SpringBootApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
@TestPropertySource("classpath:test.properties")
public class LoginNavIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void configureMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void testRightLoginAdmin() throws Exception {
        mockMvc.perform(formLogin(LOGIN_URL).user(USERNAME_ADMIN_OK).password(PASSWRD_ADMIN_OK))
            .andExpect(authenticated());
    }

    @Test
    public void testWrongLoginAdmin() throws Exception {
        mockMvc.perform(formLogin(LOGIN_URL).user(USERNAME_ADMIN_OK).password(PASSWRD_ADMIN_KO))
            .andExpect(unauthenticated());
    }

    @Test
    public void testRightLoginUser() throws Exception {
        mockMvc.perform(formLogin(LOGIN_URL).user(USERNAME_USER_OK).password(PASSWRD_USER_OK))
            .andExpect(authenticated());
    }

    @Test
    public void testWrongLoginUser() throws Exception {
        mockMvc.perform(formLogin(LOGIN_URL).user(USERNAME_USER_OK).password(PASSWRD_USER_KO))
            .andExpect(unauthenticated());
    }

    @Test
    public void testRightLoginPost() throws Exception {
        mockMvc.perform(
            post(LOGIN_URL)
                .with(csrf())
                .param(USERNAME_PARAM, USERNAME_ADMIN_OK)
                .param(PASSWORD_PARAM, PASSWRD_ADMIN_OK))
            .andExpect(authenticated());
    }

    @Test
    public void testWrongLoginPost() throws Exception {
        mockMvc.perform(
            post(LOGIN_URL)
                .with(csrf())
                .param(USERNAME_PARAM, USERNAME_ADMIN_OK)
                .param(PASSWORD_PARAM, PASSWRD_ADMIN_KO))
            .andExpect(unauthenticated());
    }

    @Test
    public void testHackCsrfLoginPost() throws Exception {
        mockMvc.perform(
            post(LOGIN_URL)
                .with(csrf().useInvalidToken())
                .param(USERNAME_PARAM, USERNAME_ADMIN_OK)
                .param(PASSWORD_PARAM, PASSWRD_ADMIN_OK))
            .andExpect(unauthenticated());
    }

    @Test
    public void testLogout() throws Exception {
        mockMvc.perform(logout(LOGOUT_URL))
            .andExpect(status().isFound())
            .andExpect(cookie().value("JSESSIONID", (String) null));
        ;
    }

    @Test
    public void testPublicRes() throws Exception {
        mockMvc.perform(get(FAVICON_URL)).andExpect(status().isOk());
    }

    @Test
    public void testPublicWebjars() throws Exception {
        mockMvc.perform(get(ANGULARJS_URL)).andExpect(status().isOk());
    }

    @Test
    public void testNotLoggedIn() throws Exception {
        mockMvc.perform(get(NOTESTAB_URL)).andExpect(status().isFound());
    }
}
