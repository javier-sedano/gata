package com.odroid.gata.integration.web.nav;

import com.odroid.gata.SpringBootApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.odroid.gata.integration.web.WebIntegrationTestUtils.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG"
        })
@TestPropertySource("classpath:test.properties")
public class UsersNavIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void configureMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void testUsersForAdmin() throws Exception {
        mockMvc.perform(
                get(USERSTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(status().isOk());
        mockMvc.perform(
                get(USERSTAB_URL).with(testUser()))
                .andDo(log())
                .andExpect(status().isForbidden());
    }

}
