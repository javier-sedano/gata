package com.odroid.gata.integration.web.nav;

import com.odroid.gata.SpringBootApp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.odroid.gata.integration.web.WebIntegrationTestUtils.*;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class,
        properties = {
                "logging.level.org.springframework.test.web.servlet.result=DEBUG"
        })
@TestPropertySource("classpath:test.properties")
public class HeaderNavIntegrationTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void configureMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void testLogoutText() throws Exception {
        mockMvc.perform(
                get(NOTESTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(xpath("//a[@href='#logout']/text()")
                        .string(LOGOUT_TEXT + USERNAME_ADMIN_OK));
        mockMvc.perform(
                get(NOTESTAB_URL).with(testUser()))
                .andDo(log())
                .andExpect(xpath("//a[@href='#logout']/text()")
                        .string(LOGOUT_TEXT + USERNAME_USER_OK));
    }

    @Test
    public void testNotesTab() throws Exception {
        mockMvc.perform(
                get(NOTESTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/notesTab']").exists());
        mockMvc.perform(
                get(NOTESTAB_URL).with(testUser()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/notesTab']").exists());
    }

    @Test
    public void testUsersTab() throws Exception {
        mockMvc.perform(
                get(NOTESTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/usersTab']").exists());
        mockMvc.perform(
                get(NOTESTAB_URL).with(testUser()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/usersTab']").doesNotExist());
    }

    @Test
    public void testNotesTabSelected() throws Exception {
        mockMvc.perform(
                get(NOTESTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/notesTab']/@class").string(containsString("w3-theme-l4")))
                .andExpect(xpath("//a[@href='/usersTab']/@class").string(containsString("w3-theme")))
                .andExpect(xpath("//a[@href='/usersTab']/@class").string(not(containsString("w3-theme-4"))));
    }

    @Test
    public void testUsersTabSelected() throws Exception {
        mockMvc.perform(
                get(USERSTAB_URL).with(testAdmin()))
                .andDo(log())
                .andExpect(xpath("//a[@href='/notesTab']/@class").string(containsString("w3-theme")))
                .andExpect(xpath("//a[@href='/notesTab']/@class").string(not(containsString("w3-theme-l4"))))
                .andExpect(xpath("//a[@href='/usersTab']/@class").string(containsString("w3-theme-l4")));
    }

}
