gataApp.directive("whenScrolledBottom", function(){
  return {
    restrict: 'A',
    scope: {
        disabled: '=whenScrolledDisabled',
        whenScrolledBottom: '&whenScrolledBottom',
        margin: '&whenScrolledBottomMargin'
    },
    link: function(scope, elem, attrs){
      var raw = elem[0];
      elem.on("scroll", function(){
        if (scope.disabled) {
          return;
        }
        var margin = (scope.margin() === undefined ? 0 : scope.margin());
        if (raw.scrollTop + raw.offsetHeight + margin >= raw.scrollHeight) {
          scope.whenScrolledBottom();
        }
      });
    }
  };
});