CREATE TABLE user_authorities (
  id BIGINT IDENTITY PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL
);

ALTER TABLE user_authorities
  ADD UNIQUE (username, authority);

CREATE TABLE users (
  id BIGINT IDENTITY  PRIMARY KEY,
  username VARCHAR(20) NOT NULL,
  hash VARCHAR(100) NOT NULL,
  enabled BOOLEAN NOT NULL,
  name VARCHAR(100) NOT NULL
);

ALTER TABLE users
  ADD UNIQUE (username);

ALTER TABLE user_authorities
  ADD FOREIGN KEY (username)
  REFERENCES users (username);

INSERT INTO users(id, username, hash, enabled, name) VALUES (1, 'a', '$2a$10$ZpLY7U9.xBkpOX0s0NT9aObCISKplNu.ZDkSVelbBixipaLiNPIcm', true, 'Admin');
INSERT INTO user_authorities(username, authority) VALUES ('a', 'ROLE_ADMIN');
INSERT INTO user_authorities(username, authority) VALUES ('a', 'ROLE_USER');
INSERT INTO users(id, username, hash, enabled, name) VALUES (2, 'b', '$2a$10$l3unhpdIZw4/bl9KMta2we06Fkd5iqbQrmgh7oTc6OA1ekxPwPDEa', true, 'Bonifacion Buendia');
INSERT INTO user_authorities(username, authority) VALUES ('b', 'ROLE_USER');
