package com.odroid.gata.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "gata")
@Data
public class GataConfiguration {

    private long pageSize;

}
