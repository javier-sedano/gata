package com.odroid.gata.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("gataJsConfiguration")
@ConfigurationProperties(prefix = "gata.js")
@Data
public class GataJsConfiguration {

    private long pageSize;

}
