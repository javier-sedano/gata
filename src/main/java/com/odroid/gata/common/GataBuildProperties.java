package com.odroid.gata.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

@Component("gataBuildProperties")
public class GataBuildProperties {

    private BuildProperties buildProperties;

    public GataBuildProperties(@Autowired(required = false) BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    public String toString() {
        if (buildProperties == null) {
            return "Gata development version";
        }
        return buildProperties.getName() + " " + buildProperties.getVersion() + " @ " + buildProperties.getTime();
    }
}
