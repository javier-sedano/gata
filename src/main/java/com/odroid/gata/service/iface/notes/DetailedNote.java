package com.odroid.gata.service.iface.notes;

import com.odroid.gata.domain.Note;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class DetailedNote {

    @NonNull
    private Long id;

    @NonNull
    private String title;

    @NonNull
    private String content;

    public static DetailedNote fromNote(Note note) {
        return new DetailedNote(note.getId(), note.getTitle(), note.getContent());
    }

}
