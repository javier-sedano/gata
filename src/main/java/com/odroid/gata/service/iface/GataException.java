package com.odroid.gata.service.iface;

import java.io.Serializable;

public abstract class GataException extends RuntimeException {
    private final String i18nCode;
    private final Serializable[] args;

    public GataException(String i18nCode, Serializable... args) {
        super(i18nCode);
        this.i18nCode = i18nCode;
        this.args = args;
    }

    public String getI18nCode() {
        return i18nCode;
    }

    public Serializable[] getArgs() {
        return args;
    }
}
