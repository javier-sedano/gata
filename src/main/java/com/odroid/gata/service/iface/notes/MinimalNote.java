package com.odroid.gata.service.iface.notes;

import com.odroid.gata.domain.Note;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class MinimalNote {

    @NonNull
    private Long id;

    @NonNull
    private String title;

    public static MinimalNote fromNote(Note note) {
        return new MinimalNote(note.getId(), note.getTitle());
    }

}
